#!/bin/python3

import math
import os
import random
import re
import sys
  
# Complete the solve function below.
def solve(s):
    arr = s.split(" ")
    out = []
    s1 = ""
    for i in range(len(arr)):
        s1 = str(arr[i])
        out.append(s1.capitalize())
    return(" ".join(out))

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    s = input()

    result = solve(s)

    fptr.write(result + '\n')

    fptr.close()
